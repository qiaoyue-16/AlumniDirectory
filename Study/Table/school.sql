/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : book

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 25/04/2021 16:31:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for school
-- ----------------------------
DROP TABLE IF EXISTS `school`;
CREATE TABLE `school`  (
  `schoolid` int NOT NULL AUTO_INCREMENT,
  `schoolname` varchar(2550) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `schoolguanwang` varchar(2550) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`schoolid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of school
-- ----------------------------
INSERT INTO `school` VALUES (1, '南京大学', 'https://www.nju.edu.cn/');
INSERT INTO `school` VALUES (2, '东南大学', 'https://www.seu.edu.cn/');
INSERT INTO `school` VALUES (5, '南京航空航天大学', 'http://www.nuaa.edu.cn/');
INSERT INTO `school` VALUES (6, '南京大学', 'https://www.nju.edu.cn/');
INSERT INTO `school` VALUES (7, '东南大学', 'https://www.seu.edu.cn/');
INSERT INTO `school` VALUES (8, '南京航空航天大学', 'http://www.nuaa.edu.cn/');
INSERT INTO `school` VALUES (9, '南京大学', 'https://www.nju.edu.cn/');
INSERT INTO `school` VALUES (10, '东南大学', 'https://www.seu.edu.cn/');
INSERT INTO `school` VALUES (11, '南京航空航天大学', 'http://www.nuaa.edu.cn/');
INSERT INTO `school` VALUES (12, '南京大学', 'https://www.nju.edu.cn/');
INSERT INTO `school` VALUES (13, '东南大学', 'https://www.seu.edu.cn/');
INSERT INTO `school` VALUES (14, '南京航空航天大学', 'http://www.nuaa.edu.cn/');

SET FOREIGN_KEY_CHECKS = 1;
