/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : book

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 25/04/2021 16:30:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for huodong
-- ----------------------------
DROP TABLE IF EXISTS `huodong`;
CREATE TABLE `huodong`  (
  `huodongId` int NOT NULL AUTO_INCREMENT,
  `huodongCreatetime` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `huodongBiaoti` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `huodongNeirong` varchar(2550) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `id` int NOT NULL,
  PRIMARY KEY (`huodongId`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  CONSTRAINT `huodong_ibfk_1` FOREIGN KEY (`id`) REFERENCES `t_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of huodong
-- ----------------------------
INSERT INTO `huodong` VALUES (2, '2021-04-20 12:27:24', '123', '1234', 1);
INSERT INTO `huodong` VALUES (3, '2021-04-20 12:33:42', '活动', '活动', 1);
INSERT INTO `huodong` VALUES (4, '2021-04-20 12:35:19', '活动2021-4-20 12:35:15', '活动2021-4-20 12:35:17', 1);
INSERT INTO `huodong` VALUES (5, '2021-04-23 16:29:40', '金秋时节的活力健身行 ——外院分工会举行', '立冬前的南京持续着秋高气爽的好天气，非常适合户外活动。11月7日，外院教职工迎来了2019年秋季学期“自然之旅”羊山湖健身行。\r\n\r\n下午1点，参加健身行的40余位教职工在侨裕楼大门前合影留念，然后在院旗的指引下，穿过被深红、金黄、浓绿等色彩渲染得格外美丽的校园，浩浩荡荡地朝着羊山湖前进。一路上，大家一边欢快地行进，一边惬意地交流。大约40分钟后，队伍来到了羊山湖边的折返点。大家在这里驻足片刻，享受煦暖的阳光和静谧的湖景。归程途中，教职工们依然欢声笑语不断，昂首阔步向前走，展现了积极、奋进的精神风貌。\r\n\r\n两点半左右，此次健身行活动圆满结束。这趟“自然之旅”不仅让教职工们放松了心情，锻炼了身体，而且也增强了团队的凝聚力、向心力和战斗力。', 1);
INSERT INTO `huodong` VALUES (6, '2021-04-23 16:31:37', '第26个“世界读书日”即将来临啦！', '4月23日读书节当天上午10点，在李文正图书馆二楼大厅将举行东南大学第十三届读书节开幕式活动，届时将发布“2020年度东南大学阅读报告”，为2020年度借阅及泡馆达人颁奖，Seal朗诵团朗诵《我心永向党:家书里的百年信仰》篇章等活动，欢迎广大师生的到来！', 1);

SET FOREIGN_KEY_CHECKS = 1;
