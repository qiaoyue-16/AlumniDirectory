/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : book

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 25/04/2021 16:30:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fankui
-- ----------------------------
DROP TABLE IF EXISTS `fankui`;
CREATE TABLE `fankui`  (
  `fankuiId` int NOT NULL AUTO_INCREMENT,
  `fankuiCreatetime` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fankuiBiaoti` varchar(2550) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `fankuiNeirong` varchar(2550) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `id` int NULL DEFAULT NULL,
  PRIMARY KEY (`fankuiId`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  CONSTRAINT `fankui_ibfk_1` FOREIGN KEY (`id`) REFERENCES `t_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of fankui
-- ----------------------------
INSERT INTO `fankui` VALUES (1, '2021-04-19 20:35:17', '你好', '我要反馈', 1);
INSERT INTO `fankui` VALUES (2, '2021-04-19 22:24:25', '页面加载时调用js函数方法', '\"\"', 1);
INSERT INTO `fankui` VALUES (3, '2021-04-19 22:25:44', '反馈跳转测试', '反馈跳转测试', 1);
INSERT INTO `fankui` VALUES (4, '2021-04-19 22:26:42', '反馈跳转测试2226', '反馈跳转测试2226', 1);
INSERT INTO `fankui` VALUES (6, '2021-04-20 12:00:37', '反馈测试', '2021-4-20 11:58:50', 1);

SET FOREIGN_KEY_CHECKS = 1;
