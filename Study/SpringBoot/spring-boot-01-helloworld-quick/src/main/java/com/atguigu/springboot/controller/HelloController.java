package com.atguigu.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 巧月十六
 * @date 2021/8/27 - 10:41
 */

// 这个类的所有方法返回的数据直接写个浏览器，（如果是对象转为json数据）
//@ResponseBody
//@Controller
@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String Hello() {
        return "hello world quick!";
    }
}
