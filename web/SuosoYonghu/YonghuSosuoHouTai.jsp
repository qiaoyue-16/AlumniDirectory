<%@ page import="com.atguigu.pojo.Book" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript" src="../script/jquery-3.5.1.js"></script>
</head>
<%@include file="../public_jsp/admin_header.jsp" %>
<link rel="stylesheet" href="head_css/public.css" type="text/css">
<link rel="stylesheet" href="public_jsp/yemianbuju.css" type="text/css">
<link rel="stylesheet" href="public_jsp/zhuyefatieanniu.css" type="text/css">
<link rel="stylesheet" href="public_jsp/pg.css" type="text/css">
<link rel="stylesheet" href="public_jsp/guanliyemian.css" type="text/css">
<body style="overflow-y: auto">
<div class="row">
    <div class="column side">
        <div>
            <img src="tupian/wallhaven-4gdogq_200x1080.png" style="width: 100%">
        </div>
    </div>

    <div class="column middle">
        <img src="tupian/wallhaven-4gkvy3_1920x400.png" style="width: 100%">
        <div class="card">
            <form action="client/bookServlet_qiantai" method="get">
                <input type="hidden" name="action" value="pageByIdHou">
                搜索用户（ID）<input id="yonghuid" type="text" name="yonghuid" value="${param.yonghuid}">
                <input type="submit" value="搜索"/>
            </form>
        </div>
        <div class="card">
            <h1>用户搜索</h1>
            <div id="main" style="width: 100%;">
                    <form action="manager/xunxiaoguanliSerlvet" method="post" style="width: 100%;">
                    <table>
                        <tr>
                            <td>用户编号</td>
                            <td>用户名</td>
                            <td>用户昵称</td>
                            <td>用户学校</td>
                            <td>用户班级</td>
                            <%--操作--%>
                            <c:if test="${sessionScope.user.quanxian eq 1}"><td colspan="1">操作</td></c:if>
                            <c:if test="${sessionScope.user.quanxian eq 2}"><td colspan="1">操作</td></c:if>
                            <c:if test="${sessionScope.user.quanxian eq 3}"><td colspan="1">操作</td></c:if>
                            <c:if test="${sessionScope.user.quanxian eq 23}"><td colspan="1">操作</td></c:if>
                        </tr>
                        <%--        books 要和 BookServlet req.setAttribute 中保持一致--%>
                        <c:forEach items="${requestScope.page.items}" var="user">
                            <tr>
                                <td>No.${user.id}</td>
                                <td>${user.username}</td>
                                <td>${user.nickname}</td>
                                <td>
                                    <c:if test="${not empty user.xuexiao}">
                                        ${user.xuexiao}
                                    </c:if>
                                    <c:if test="${empty user.xuexiao}">
                                        无所属学校
                                    </c:if>
                                </td>
                                <td>
                                    <c:if test="${not empty user.banji}">
                                        ${user.banji}
                                    </c:if>
                                    <c:if test="${empty user.banji}">
                                        无所属班级
                                    </c:if>
                                </td>
                                <c:if test="${sessionScope.user.quanxian eq 1}">
                                <td><a href="manager/yonghuguanliServlet?action=getUser&id=${user.id}&pageNo=${requestScope.page.pageNo}">修改</a>
                                </c:if>
                                <c:if test="${sessionScope.user.quanxian eq 2}">
                                    <a href="manager/xunxiaoguanliSerlvet?action=getUser&id=${user.id}&pageNo=${requestScope.page.pageNo}&xuexiao=${sessionScope.user.xuexiao}">修改</a>
                                </c:if>
                                <c:if test="${sessionScope.user.quanxian eq 3}">
                                    <a href="manager/banjiSerlvet?action=getUser&id=${user.id}&pageNo=${requestScope.page.pageNo}&xuexiao=${sessionScope.user.xuexiao}&banji=${sessionScope.user.banji}">修改</a>
                                    </c:if>
                                <c:if test="${sessionScope.user.quanxian eq 23}"><td colspan="1"><a href="manager/banjiSerlvet?action=getUser&id=${user.id}&pageNo=${requestScope.page.pageNo}&xuexiao=${sessionScope.user.xuexiao}&banji=${sessionScope.user.banji}">修改</a></td></c:if>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="6">
                                <%@include file="/public_jsp/page_nav.jsp"%>
                            </td>
                        </tr>
                    </table>
                    </form>
            </div>
        </div>

    </div>

    <div class="column side">
        <img src="tupian/wallhaven-4gdogq_200x1080_right.png" style="width: 100%">
    </div>
</div>
</body>
</html>
